===Regular Bonds===
* Fixed Coupon
* Maturity Date is fixed
* May or may not have a call / put option
* Generally issued at Par

===Callable Bonds===
* Bonds that allow the issuer to exercise a call option and redeem the bonds prior to its original maturity date
* Since these options are not separated from the original bond issue, they are also called embedded options
* The call option provides the issuer the option to redeem a bond, if interest rates decline, and re-issue the bonds at a lower rate

===Puttable Bonds===
* Provide the investor with the right to seek redemption from the issuer, prior to the maturity date
* A put option provides the investor the right to sell a low coupon-paying bond to the issuer, and invest in higher coupon paying bonds, if interest rates move up